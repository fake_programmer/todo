@extends('layouts.app')

@section('content')

    <h1 class="text-center my-5"> Create Todo</h1>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-default">
                <div class="card-header">Create New Todo</div>
                <div class="card-body">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul class="list-group">
                                @foreach($errors->all() as $error)
                                    <li class="list-group-item">
                                        {{ $error }}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{route('storeTodos')}}" method="POST">
                        @csrf
                        <div class="from-group my-2">
                            <input type="text" class="form-control" name="name"
                                   placeholder=" Please Enter the Todos  Name">
                        </div>

                        <div class="from-group">
                            <textarea name="description" id="description" cols="74" rows="10"></textarea>
                        </div>
                        <div class="from-group text-center my-2">
                            <button type="submit" class="btn btn-success"> Create Todo</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>


@endsection