@extends('layouts.app')

@section('title')
    Todo List
@endsection

@section('content')


    <h1 class="text-center my-5">To Do Apps</h1>
    <div class="row justify-content-center">
        <div class="col-md-8 offset-2">
            <div class="card card-defult">

                <div class="card-header">
                    Todos
                </div>
                <div class="card-body">
                    <Ul class="list-group">
                        @foreach($todos as $todo)
                            <li class="list-group-item">
                                {{ $todo->name }}
                                <a href="todos/{{$todo->id}}" class="btn btn-primary btn-sm float-right">view</a>
                            </li>
                        @endforeach
                    </Ul>

                </div>
            </div>


        </div>
    </div>
@endsection